// import Raven from "raven-js";

function init() {
  // Raven.config(
  //   "https://1fc9bfb6970a47c09afe8daaf945b6b2@o463500.ingest.sentry.io/5468555",
  //   {
  //     release: "1-0-0",
  //     environment: "development-test",
  //   }
  // ).install();
}

function log(error) {
  console.log(error);
  // Raven.captureException(error);
}

export default { init, log };
